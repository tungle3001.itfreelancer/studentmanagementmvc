package com.company.Model;

import java.util.Date;

public class Student {
    public Student(int code, String name, Date birthDate) {
        this.code = code;
        this.name = name;
        this.birthDate = birthDate;
    }

    public Student() {
    }

    private int code;

    public String name;

    private Date birthDate;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {

        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name==null){
            System.out.println("Name is not empty!, input again");
        }else{
            this.name = name;
        }

    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {

        this.birthDate = birthDate;
    }

}
